{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE BangPatterns        #-}

module Main where
    import Network
    import System.IO
    import System.Directory
    import Control.Monad
    import Control.Concurrent
    import System.Environment
    import Data.ByteString (hGet, hPut)
    import Data.List.Split
    import Data.List
    import Control.Exception

    --sPort :: Integer
    --sPort = 15780

    main :: IO()
    main = withSocketsDo $ do
        args <- getArgs
        let catalog = head args
        let sport = read (last args) :: Integer
        server <- listenOn $ PortNumber $ fromIntegral sport
        putStrLn $ "Listen on " ++ show sport
        forever $ do
            (chandle, chost, cport) <- accept server
            putStrLn $ "Got connection on '" ++ chost ++ " : " ++ show cport ++ "'"
            forkIO $ work chandle chost catalog


    work :: Handle -> String -> String-> IO()
    work chandle chost current = do
        do
            hSetBuffering chandle LineBuffering
            cmd <- hGetLine chandle
            putStrLn ("Command from '" ++ chost ++ "': " ++ cmd)
            case cmd of
                "dir" -> do
                    content <- getDirectoryContents current >>= return . filter (`notElem` [".", ".."])
                    sendDirContent chandle content
                    work chandle chost current
                "get" -> do
                    filename <- hGetLine chandle
                    sendFile chandle current filename
                    putStrLn ("Done for '" ++ filename ++ "'")
                    work chandle chost current
                "pwd" -> do
                    hPutStrLn chandle current
                    work chandle chost current
                "cd" -> do 
                    path <- hGetLine chandle

                    case path of
                        ".." -> do
                            let cat_lst = removeItem "" (splitOn "\\" current)
                            let buf = head cat_lst
                            let p_len = length (init cat_lst)
                            case compare p_len 1 of
                                LT -> do
                                    let new_path = buf ++ "\\"
                                    work chandle chost new_path
                                EQ -> do
                                    let new_path = buf ++ "\\"
                                    work chandle chost new_path
                                GT -> do
                                    let new_path = intercalate "\\" $ init cat_lst
                                    work chandle chost new_path
                        _ -> do
                            let cat_lst = removeItem "" (splitOn "\\" current)
                            let old_path = intercalate "\\" cat_lst
                            let new_path = old_path ++ "\\" ++ path
                            work chandle chost new_path
                    
                "quit" -> do
                    putStrLn $ "'" ++ chost ++ "' disconnected!"
                _ -> do
                    putStrLn $ "Wrong command from " ++ chost ++ "!"
                    work chandle chost current
        hClose chandle

    sendFile :: Handle -> String -> String -> IO()
    sendFile chandle path filename = do
        handle (
            \(e :: SomeException) ->  do 
                putStrLn "File open error!"
                hPutStrLn chandle $ show 1
                return ()
                ) $ do
                withFile (path ++ "\\" ++ filename) ReadMode $ \hfile -> do
                    hPutStrLn chandle $ show 0
                    size <- hFileSize hfile
                    hPutStrLn chandle $ show size
                    readToSend chandle hfile

    readToSend :: Handle -> Handle -> IO()
    readToSend chandle fhandle = do
        eof_p <- hIsEOF fhandle
        case eof_p of
            True -> do
                putStrLn "Transmission done!"
            False -> do
                buf <- hGet fhandle 2048
                hPut chandle buf
                readToSend chandle fhandle 

    getlines :: Handle -> IO [String]
    getlines h = hGetContents h >>= return . lines

    sendDirContent :: Handle -> [FilePath] -> IO()
    sendDirContent chandle content@(h:[]) = do
        hPutStrLn chandle h
        hPutStrLn chandle "\r"

    sendDirContent chandle content = do
        hPutStrLn chandle $ head content
        sendDirContent chandle $ tail content

    removeItem _ [] = []
    removeItem x (y:ys) 
            | x == y    = removeItem x ys    
            | otherwise = y : removeItem x ys