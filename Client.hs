module Main where
    import Network
    import System.IO
    import System.Environment
    import Data.ByteString (hGet, hPut)

    --sPort :: Integer
    --sPort = 15780

    --sHost :: String
    --sHost = "127.0.0.1"

    main :: IO()
    main = withSocketsDo $ do
        args <- getArgs
        let shost = head args
        let sport = read (last args) :: Integer
        shandle <- connectTo shost $ PortNumber $ fromIntegral sport
        hSetBuffering shandle LineBuffering
        loop shandle
        hClose shandle
    
    loop :: Handle -> IO()
    loop shandle = do
        instr <- getLine
        let rwords = words instr :: [String]
        let cmd = head rwords
        case cmd of
            "dir" -> do
                hPutStrLn shandle cmd
                getDirContent shandle
                loop shandle
            "get" -> do
                hPutStrLn shandle cmd
                let filename = last rwords
                hPutStrLn shandle filename
                err <- hGetLine shandle
                case err of
                    "0" -> do
                        fhandle <- openFile filename WriteMode
                        size <- hGetLine shandle
                        putStrLn ("File size: " ++ size)
                        let size_int = read $ size :: Int
                        getFile shandle fhandle size_int
                        putStrLn ("Got '" ++ filename ++ "'")
                        loop shandle
                    _ -> do
                        putStrLn "File open error!"
                        loop shandle
            "cd" -> do
                hPutStrLn shandle cmd
                let path = last rwords
                hPutStrLn shandle path
                loop shandle
            "pwd" -> do
                hPutStrLn shandle cmd
                path <- hGetLine shandle
                putStrLn ("Server path: " ++ path)
                loop shandle
            "quit" -> do
                hPutStrLn shandle cmd
            _ -> do
                putStrLn "Wrong command!"
                loop shandle

    
    getFile :: Handle -> Handle -> Int -> IO()
    getFile shandle fhandle size = do

        case compare size 2048 of 
            LT -> do
                buf <- hGet shandle size                
                hPut fhandle buf
                hFlush fhandle
                hClose fhandle
            EQ -> do
                buf <- hGet shandle size
                hPut fhandle buf
                hFlush fhandle
                hClose fhandle
            GT -> do
                buf <- hGet shandle 2048
                putStrLn ("Left: " ++ (show size) ++ " bytes.")
                hPut fhandle buf
                getFile shandle fhandle (size - 2048)

{--
        content <- hGetLine shandle
        case content of
            "\r" -> do
                hPutStrLn fhandle ""
            _ -> do 
                hPutStrLn fhandle content
                getFile shandle fhandle
--}
    getDirContent :: Handle -> IO()
    getDirContent shandle = do
        answer <- hGetLine shandle
        case answer of
            "\r" -> do
                putStrLn ""
            _ -> do
                putStrLn answer
                getDirContent shandle
